package com.kelompok5;

public class Karyawan {
    protected int salary;
    protected id_card IdCard;
    protected ParkingCard KartuParkir;

    public int totalSalary(int id_card_no){
        return this.salary * id_card_no;
    }

    public void setIdCard(id_card IdCard){
        this.IdCard = IdCard;
    }
    public void setParkingCard(ParkingCard KartuParkir){
        this.KartuParkir = KartuParkir;
    }

    public id_card getIdCard(){
        return this.IdCard;
    }

    public ParkingCard getKartuParkir(){
        return this.KartuParkir;
    }
}