package com.kelompok5;

public class ParkingCard{
    private int id_parking_card, id_card_no;
    
    public ParkingCard(int id_parking_card, int id_card_no){
        this.id_parking_card = id_parking_card;
        this.id_card_no = id_card_no;
    }

    int getId_parking_card(){
        return this.id_parking_card;
    }
}