package com.kelompok5;

import java.util.ArrayList;

public class App
{
    public static void main(String[] args) {

        // Pegawai
        Pegawai pegawai1 = new Pegawai();
        // Pembuatan ID CARD pegawai pertama lewat class id_card.
        id_card IdCard = new id_card(1, "Joko");
        // Pembuatan Kartu Parkir Pegawai. Nomor IdCard diambil dari object IdCard.
        ParkingCard KartuParkir = new ParkingCard(12, IdCard.getIdCardNo());
        // Id Card dan Kartu Parkir diberikan kepada pegawai1.
        pegawai1.setIdCard(IdCard);
        pegawai1.setParkingCard(KartuParkir);

        Pegawai pegawai2 = new Pegawai();
        id_card IdCard2 = new id_card(2, "Dana");

        ParkingCard KartuParkir2 = new ParkingCard(22, IdCard2.getIdCardNo());
        pegawai2.setIdCard(IdCard2);
        pegawai2.setParkingCard(KartuParkir2);

        Pegawai pegawai3 = new Pegawai();
        id_card IdCard3 = new id_card(2, "Hafizh");

        ParkingCard KartuParkir3 = new ParkingCard(32, IdCard3.getIdCardNo());
        pegawai3.setIdCard(IdCard3);
        pegawai3.setParkingCard(KartuParkir3);
        // Pegawai End

        // Manager
        Manager manager1 = new Manager();
        id_card IdCard4 = new id_card(4, "Aldo");

        ParkingCard KartuParkir4 = new ParkingCard(42, IdCard4.getIdCardNo());
        manager1.setIdCard(IdCard4);
        manager1.setParkingCard(KartuParkir4);

        Manager manager2 = new Manager();
        id_card IdCard5 = new id_card(5, "Yona");

        ParkingCard KartuParkir5 = new ParkingCard(52, IdCard5.getIdCardNo());
        manager2.setIdCard(IdCard5);
        manager2.setParkingCard(KartuParkir5);
        // Manager End

        // Untuk membuat ArayList dari kelas karyawan.
        ArrayList<Karyawan>Emps = new ArrayList<Karyawan>();
        Emps.add(pegawai1);
        Emps.add(pegawai2);
        Emps.add(pegawai3);
        Emps.add(manager1);
        Emps.add(manager2);

        for (int i = 0 ; i<Emps.size() ; i++) {
            System.out.println("Parking Card Id:" + Emps.get(i).getKartuParkir().getId_parking_card() + " | " + "Nama:" + Emps.get(i).getIdCard().getName());
            System.out.println("Nama: " + "" + Emps.get(i).getIdCard().getName() + " | " + "Salary:" + " " + Emps.get(i).totalSalary(Emps.get(i).getIdCard().getIdCardNo()) + "\n");
        }
    }
}
