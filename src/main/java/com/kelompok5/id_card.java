package com.kelompok5;

public class id_card {
    private int id_card_no;
    private String name;
    
    public id_card(int id_card_no, String name){
        this.id_card_no = id_card_no;
        this.name = name;
    };

    public void setName(String name){
        this.name = name;
    }

    int getIdCardNo(){
        return this.id_card_no;
    }

    String getName(){
        return this.name;
    }
}